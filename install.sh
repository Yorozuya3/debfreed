#Script to install debfreed
if [ -d "/home/$USER/.debfreed" ];
then
  echo "ERROR: Directory exist, you already installed it?"
else
  mypath=$(pwd)
  #Copy files
  echo "Copying files..."
  cp -r $mypath /home/$USER/.debfreed
  rm /home/$USER/.debfreed/install.sh
  echo "Done!"
  #Adding program to path
  echo "Adding debfreed to PATH..."
  echo "#Debfreed path" >> /home/$USER/.bashrc
  echo "export PATH=\$PATH:/home/$USER/.debfreed/source-code" >> /home/$USER/.bashrc
  source /home/$USER/.bashrc
  echo 'Done!'
fi
