# debfreed

Debfreed is a tool to detect non-free software on Debian based distros showing its priority and allowing you to remove it easily.
***
#### How install it?
 Download the package and execute install.sh script. To uninstall execute uninstall.sh to be sure to remove all files.

#### How to use it?
 USAGE:   debfreed [OPTION]
 Available options:

	--show     show non-free packages installed

	--purge    remove all non-free software of your computer

		purge options:
		      0 : Delete all non-free package installed
		      1 : Exclude drivers packages
	--help     show a command list and version of program



